const app = {

  // Initialization
  initialize: function() {
    this.bindEvents();
  },

  // Bind event listeners
  // Common: 'load', 'deviceready', 'offline', 'online', 'backbutton'
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  // Handle device-ready event
  onDeviceReady: function() {

    // Initialize touch remap
    Touchremap.initialize();

    // Initialize app driver.
    // Prepares event listeners and more stuff
    // that has to be available before any plugins are loaded.
    Driver.initialize();

    // Initialize view engine
    View.initialize();

    // Initialize AdMob ad manager with advertiser id
    AdManager.initialize({
      banner: 'ca-app-pub-3945575180168978/6857865707',
    });

    // Initialize theme engine
    Themer.initialize();

    // Initialize developer stats
    DevStats.initialize();

    // Disable custom text zoom.
    // Avoids scaling issues and broken layouts.
    if (window.MobileAccessibility) {
      window.MobileAccessibility.usePreferredTextZoom(false);
    }

    // Enable test ads by default
    AdManager.debug();

    // Do some magic to show production ads
    // when the app is built in release mode.
    if (cordova.plugins.IsDebug) {
      cordova.plugins.IsDebug.getIsDebug(
        isDebug => {
          if (!isDebug) AdManager.production();
          evee.emit('ad-system-ready');
        },
        err => {
          evee.emit('ad-system-ready');
        }
      );
    } else {
      evee.emit('ad-system-ready');
    }

    // Start the driver
    Driver.start();

    // Delegate connectivity events to evee
    if (navigator.connection.type === Connection.NONE) evee.emit('device-offline');
    document.addEventListener('offline', () => evee.emit('device-offline'), false);
    document.addEventListener('online', () => evee.emit('device-online'), false);

    // Register back button handler
    document.addEventListener('backbutton', () => {
      if (View.__store.currentView === View.__store.defaultView) {
        navigator.app.exitApp();
      } else {
        View.presentLast();
      }
    });
  },
  __store: {},
};

// Initialize the app
app.initialize();
