window.Coincap = {
  fetch_page: function(symbol, cb) {
    const url = `http://coincap.io/page/${symbol}`;
    fetch(url, {cache: 'no-store'})
      .then(resp => resp.json())
      .then(json => cb(json))
      .catch(err => console.log(err));
  },
  fetch_history: function(symbol, cb) {
    const url = `https://coincap.io/history/1day/${symbol}`;
    fetch(url, {cache: 'no-store'})
      .then(resp => resp.json())
      .then(json => cb(json))
      .catch(err => console.log(err));
  },
  fetch_every: function(symbol, ms, cb) {
    this.fetch_page(symbol, cb);
    setInterval(async () => {
      this.fetch_page(symbol, cb);
    }, ms);
  },
};
