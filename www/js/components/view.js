/*
 * <component name=view>
 *
 * A simple view manager.
 *
 * <dependency name=animator>
 * <dependency name=evee>
 *
 * <event name=view-changing>
 * <event name=view-changed>
 *
 * </component>
 */

window.View = {

  // Initialization.
  initialize: function() {

    // Initialize event handlers
    this.__initHandlers();

    // Set default view as current view
    const viewDefault = document.querySelector(`[view-default]`);
    this.__store.defaultView = viewDefault.getAttribute('view');
    this.__store.currentView = viewDefault.getAttribute('view');
  },

  // Transition to the last view.
  presentLast: function() {

    // Test if history contains views
    if (this.__store.history.length === 0) {

      // Present default vuiew
      this.present(this.__store.defaultView);
    } else {

      // Get last view
      const lastView = this.__store.history.pop();

      // Present last view
      this.present(lastView, true);
    }
  },

  // Transition to specified view.
  present: function(viewToName, preventUpdateHistory) {

    // Get active view
    const viewFromName = this.__store.currentView;

    // Return if target is active view
    // if (viewFromName === viewToName) return;

    // Get active and target view elements
    const viewFrom  = document.querySelector(`[view=${viewFromName}]`);
    const viewTo    = document.querySelector(`[view=${viewToName}]`);

    // Update current view
    this.__store.currentView = viewToName;

    // Fire event
    evee.emit('view-changing', {
      viewFrom: viewFromName,
      viewName: viewToName,
    });

    // Fade active view out
    window.Animator.hide(viewFrom, () => {

      // Scroll back to 0,0
      window.scrollTo(0, 0);

      // Fade target view in
      window.Animator.show(viewTo, () => {

        // Update current view
        this.__store.currentView = viewToName;

        // Fire event
        evee.emit('view-changed', {
          viewFrom: viewFromName,
          viewName: viewToName,
        });
      });
    });

    // Add view to history
    if (preventUpdateHistory === void 0) {
      this.__store.history.push(viewFromName);
    }
  },

  // Initialize handlers.
  __initHandlers: function() {

    // Iterate over elements with view-present attribute
    for (const trigger of document.querySelectorAll('[view-present]')) {

      // Add click event handler
      trigger.addEventListener('click', e => {

        // Get the value of the view-present attribute
        const viewTarget = trigger.getAttribute('view-present');

        // Transition to the target view
        this.present(viewTarget);

        // Prevent default event handler
        e.preventDefault();
      }, false);
    }

    // Iterate over elements with view-present-last attribute
    for (const trigger of document.querySelectorAll('[view-present-last]')) {

            // Add click event handler
            trigger.addEventListener('click', e => {

              // Transition to the last view
              this.presentLast();

              // Prevent default event handler
              e.preventDefault();
            }, false);
          }
  },

  // Storage object
  __store: {
    history: [],
  },
};
