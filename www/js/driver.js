const SYMBOL = 'ETH';
const FETCH_TIMEOUT = 20000; // 20 sec

window.Driver = {
  initialize: initialize,
  start: driverMain,
  __store: {
    wasOffline: false,
  },
};

function initialize() {
  window.addEventListener('orientationchange', () => {
    Plotly.purge(document.querySelector('.plot'));
  });
  window.addEventListener('orientationchangeend', () => {
    replotTrend();
  });
  window.addEventListener('resize', () => {
    replotTrend();
  });
  let devClicks = 0;
  document.querySelector('[view=settings] .logo').addEventListener('click', () => {
    if (devClicks++ === 20) {
      devClicks = 0;
      View.present('devstats');
    }
  }, false);
  evee.on('ad-system-ready', () => {
    AdManager.showBannerAd();
    AdManager.ready();
  });
  evee.on('ad-present', () => {
    replotTrend();
  });
  evee.on('ad-banner-success', () => {
    replotTrend();
  });
  evee.on('device-offline', () => {
    Driver.__store.wasOffline = true;
    const el = document.querySelector('.banner-offline');
    Animator.show(el);
  });
  evee.on('device-online', () => {
    Coincap.fetch_page(SYMBOL, fetchCallback);
    const el = document.querySelector('.banner-offline');
    Animator.hide(el);
    if (!Driver.__store.wasOffline) return;
    Driver.__store.wasOffline = false;
    AdManager.showBannerAd();
  });
  evee.on('view-changing', event => {
    if (event.data.viewName === View.__store.defaultView) {
      Plotly.purge(document.querySelector('.plot'));
    } else if (event.data.viewName === 'devstats') {
      DevStats.updateStats();
    }
  });
  evee.on('view-changed', event => {
    if (event.data.viewName === View.__store.defaultView) {
      replotTrend();
    }
  });
}

function driverMain() {
  Coincap.fetch_page(SYMBOL, data => {
    fetchCallback(data);
  });
  setTimeout(() => {
    Coincap.fetch_every(SYMBOL, FETCH_TIMEOUT, fetchCallback);
  }, FETCH_TIMEOUT);
}

function fetchCallback(data) {
  Coincap.fetch_history(SYMBOL, history => {
    Driver.__store.cachedHistory = history;
    presentData(data);
    plotTrend(history);
  });
  if (changed(data)) {
    playAnimation(data);
  }
}

function changed(data) {
  const defaultData = {
    supply: undefined,
    volume: undefined,
    price: undefined,
    market_cap: undefined,
    cap24hrChange: undefined,
    price_usd: undefined,
  };
  const lastData = Object.assign({}, defaultData, Driver.__store.lastData);
  const currData = Object.assign({}, defaultData, data);
  return false
    || currData.supply        !== lastData.supply
    || currData.volume        !== lastData.volume
    || currData.price         !== lastData.price
    || currData.market_cap    !== lastData.market_cap
    || currData.cap24hrChange !== lastData.cap24hrChange
    || currData.price_usd     !== lastData.price_usd
    || false;
}

function playAnimation(data) {
  const logo = document.querySelector('.logo');
  if (logo.classList.contains('animate'))
    logo.classList.remove('animate');
  logo.classList.add('animate');
}

function presentData(data) {
  const price = document.querySelector('.price');
  const priceValue = document.createElement('div');
  const priceChange = document.createElement('div');
  priceValue.classList.add('value');
  priceValue.textContent = data.price.toLocaleString();
  priceChange.classList.add('change');
  priceChange.innerHTML = `
    <div class="top">24hr change</div>
    <div class="bottom">${data.cap24hrChange}%</div>
  `.trim();
  if (data.cap24hrChange > 0) {
    priceChange.classList.add('trend--up');
    priceChange.classList.remove('trend--down');
  } else {
    priceChange.classList.add('trend--down');
    priceChange.classList.remove('trend--up');
  }
  if (!price.hasChildNodes()) {
    price.appendChild(priceValue);
    price.appendChild(priceChange);
  } else {
    price.childNodes[0].replaceWith(priceValue);
    price.childNodes[1].replaceWith(priceChange);
  }
  document.querySelector('.overview').innerHTML = `
  <div class="row">
    <span class="label">Market Cap</span>
    <span class="value">$${data.market_cap.toLocaleString()}</span>
  </div>
  <div class="row">
    <span class="label">24h Volume</span>
    <span class="value">$${data.volume.toLocaleString()}</span>
  </div>
  <div class="row">
    <span class="label">Supply</span>
    <span class="value">${data.supply.toLocaleString()}</span>
  </div>
  `.trim();
}

function replotTrend() {
  plotTrend(Driver.__store.cachedHistory);
}

function plotTrend(_history) {
  const history = Object.assign({}, _history);

  // Remove the last value (latest)
  history.market_cap.splice(-1, 1);
  history.price.splice(-1, 1);

  // Get min and max values
  let min_time = Math.min(...history.market_cap.map(pair => pair[0]));
  let max_time = Math.max(...history.market_cap.map(pair => pair[0]));
  let min_market_cap = Math.min(...history.market_cap.map(pair => pair[1]));
  let max_market_cap = Math.max(...history.market_cap.map(pair => pair[1]));
  let min_price = Math.min(...history.price.map(pair => pair[1]));
  let max_price = Math.max(...history.price.map(pair => pair[1]));

  /*
  * Smoothens the graph by removing specific data points from the array.
  *
  * Technique:
  * Iterates over the data points in a linear fashion,
  * maintaining a limited view into the last n data points.
  * The view is then used to produce an OHLC candle.
  * If the current closing price is higher than the last high price,
  * or the current opening price is lower than the last low price,
  * the current price value is appended to the resulting data point array.
  *
  * This method is highly efficient in removing unnecessary data points while
  * keeping important data points intact, and produces amazing results
  * in comparison to other methods such as a biased linear search.
  */
  const smartScale = (arr, viewSize, multiScale, last) => {
    let view = [];
    const result = !!multiScale ? [] : [arr[0]];
    let lastOhlc = !!multiScale ? last : [0, 0, 0, 0];
    for (let i = 0; i < arr.length; i++) {
      const elem = arr[i];
      const price = elem[1];
      view.push(price);
      if (view.length < viewSize) {
        continue;
      }
      const ohlc = [view.shift(), Math.max(...view), Math.min(...view), price];
      const [currOpen, currHigh, currLow, currClose] = ohlc;
      const [lastOpen, lastHigh, lastLow, lastClose] = lastOhlc;
      if (currClose > lastHigh || currOpen < lastLow) {
        result.push(elem);
      }
      lastOhlc = ohlc;
    }
    if (result.length === 0 || result[result.length - 1][1] !== arr[arr.length - 1][1]) {
      result.push(arr[arr.length - 1]);
    }
    return !!multiScale ? [result, lastOhlc] : result;
  };

  // Utility function to smooth the trace with varying factors
  const smartMultiScale = (arr, viewSizes) => {
    let result = arr;
    let ohlc = [0, 0, 0, 0];
    for (const viewSize of viewSizes) {
      [result, ohlc] = smartScale(result, viewSize, true, ohlc);
    }
    result.unshift(arr[0]);
    return result;
  };

  // Smooth traces
  history.price = smartMultiScale(history.price, [5]); // 3, 3, 3, 2
  history.market_cap = smartMultiScale(history.market_cap, [3]); // 3, 2

  // Utility function to round numbers to significant digits
  const crazyRound = (n, c, up) => {
    const s = Math.round(n).toString();
    if (s.length <= c) return n;
    const round = up ? (Number(s[c]) > 0 ? 1 : 0) : 0;
    const first = (Number(s.slice(0, c)) + round).toString();
    return Number(first + '0'.repeat(s.length - c));
  };

  // Utility function to extract styles
  const getStyle = (sel, prop) => {
    const el = document.querySelector(sel);
    let result;
    if (el.currentStyle)
      result = el.currentStyle[prop];
    else if (window.getComputedStyle)
      result = document.defaultView.getComputedStyle(el, null).getPropertyValue(prop);
    return result;
  };
  const getPlotStyle = (mapping, prop) => {
    return getStyle(`.plot-color-map > [map=${mapping}]`, prop);
  };

  // Market cap plot trace
  const traceMarketCap = {
    type: 'scatter',
    mode: 'lines',
    name: 'Market Cap',
    x: history.market_cap.map(pair => new Date(pair[0])),
    y: history.market_cap.map(pair => pair[1]),
    line: {
      shape: 'spline',
      smoothing: 1,
      width: 3,
      color: getPlotStyle('trace-marketcap', 'color'),
    },
    hoveron: 'points+fills',
    hoverinfo: 'x+y',
    hoverlabel: {
      font: {
        family: getStyle('body', 'font-family'),
      },
    },
    fill: 'tonexty',
    fillcolor: getPlotStyle('trace-marketcap', 'background-color'),
  };

  // Price plot trace
  const tracePriceUsd = {
    type: 'scatter',
    mode: 'lines',
    name: 'Price (USD)',
    x: history.price.map(pair => new Date(pair[0])),
    y: history.price.map(pair => pair[1]),
    yaxis: 'y2',
    line: {
      shape: 'spline',
      smoothing: 1,
      width: 3,
      color: getPlotStyle('trace-price', 'color'),
    },
    hoveron: 'points+fills',
    hoverinfo: 'x+y',
    hoverlabel: {
      font: {
        family: getStyle('body', 'font-family'),
      },
    },
    fill: 'tonexty',
    fillcolor: getPlotStyle('trace-price', 'background-color'),
  };

  // Make sure that min and max values are displayed properly
  let matchCount = ((min, max) => {
    let i = 0;
    while (i < Math.min(min.length, max.length)) {
      if (min[i] !== max[i++]) break;
    }
    return ++i;
  })(String(min_market_cap).split(''), String(max_market_cap).split(''));
  min_market_cap = crazyRound(min_market_cap, matchCount, true);
  max_market_cap = crazyRound(max_market_cap, matchCount, false);

  // Define plot layout
  const layout = {
    showlegend: true,
    dragmode: 'zoom',
    hovermode: 'closest',
    hoverlabel: {
      bgcolor: 'white',
    },
    font: {
      family: getStyle('body', 'font-family'),
      color: getStyle('body', 'color'),
    },
    legend: {
      orientation: 'h',
      xanchor: 'right',
      yanchor: 'bottom',
      x: 0.95,
      y: 0.05,
      bgcolor: getPlotStyle('legend', 'background-color'),
      font: {
        family: getStyle('body', 'font-family'),
        color: getPlotStyle('legend', 'color'),
      }
    },
    margin: {
      l: 0,
      r: 0,
      t: 0,
      b: 0,
      autoexpand: false,
    },
    paper_bgcolor: getStyle('body', 'background-color'),
    plot_bgcolor: getStyle('body', 'background-color'),
    yaxis: {
      visible: false,
      fixedrange: true,
      showgrid: false,
    },
    yaxis2: {
      visible: false,
      fixedrange: true,
      showgrid: false,
      overlaying: 'y',
      side: 'right',
    },
    xaxis: {
      visible: false,
      fixedrange: true,
      showgrid: false,
      type: 'date',
    },
  };
  const options = {
    showLink: false,
    staticPlot: false,
    displayModeBar: false,
    displaylogo: false,
    scrollZoom: false,
    editable: false,
  };
  const traces = [traceMarketCap, tracePriceUsd];
  const elem = document.querySelector('.plot');
  Plotly.newPlot(elem, traces, layout, options);
}
